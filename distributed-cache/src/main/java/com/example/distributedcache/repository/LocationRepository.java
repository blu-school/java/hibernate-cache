package com.example.distributedcache.repository;

import com.example.distributedcache.domain.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository< Location, Long >
{
}
