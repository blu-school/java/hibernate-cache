package com.example.distributedcache.config;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.NearCacheConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CacheConfiguration
{

//   @Bean
//   public HazelcastInstance hazelcastInstance()
//   {
//      ClientConfig config = new ClientConfig();
//      config.setInstanceName( "java_guild_demo_client" );
////      config.addNearCacheConfig( nearCacheConfig() );
//      return HazelcastClient.newHazelcastClient( config);
//   }

   @Bean
   public HazelcastInstance hazelcastInstance()
   {
      Config config = new Config();
      config.setInstanceName( "java_guild_demo" );
      config.addMapConfig( initializeDomainMapConfig() );
      return Hazelcast.newHazelcastInstance(config);
   }

   private MapConfig initializeDomainMapConfig()
   {
      MapConfig mapConfig = new MapConfig("com.example.distributedcache.domain.*");
      mapConfig.setTimeToLiveSeconds(360);
      return mapConfig;
   }

   private NearCacheConfig nearCacheConfig() {
      NearCacheConfig nearCacheConfig = new NearCacheConfig();
      nearCacheConfig.getEvictionConfig().setSize( 2 );
      return nearCacheConfig;
   }

}
