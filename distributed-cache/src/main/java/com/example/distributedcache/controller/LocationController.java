package com.example.distributedcache.controller;

import com.example.distributedcache.domain.Location;
import com.example.distributedcache.repository.LocationRepository;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LocationController implements InitializingBean
{

   private final LocationRepository locationRepository;

   public LocationController( final LocationRepository locationRepository )
   {
      this.locationRepository = locationRepository;
   }

   @GetMapping( "/location/{id}" )
   public Location getLocationById( @PathVariable Long id )
   {
      return locationRepository.findById( id ).orElseThrow();
   }

   @Override
   public void afterPropertiesSet() throws Exception
   {
      locationRepository.saveAll(
         List.of( new Location( "Sofia" ),
                  new Location( "Plovdiv" ),
                  new Location( "Varna" ),
                  new Location( "Burgas" )
         ) );
   }
}
