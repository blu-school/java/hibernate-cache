package com.example.distributedcache.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache( usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE )
public class Location
{
   public Location()
   {
   }

   public Location( final String name )
   {
      this.name = name;
   }

   @Id
   @GeneratedValue( strategy = GenerationType.IDENTITY )
   private Long id;

   private String name;

   public Long getId()
   {
      return id;
   }

   public void setId( final Long id )
   {
      this.id = id;
   }

   public String getName()
   {
      return name;
   }

   public void setName( final String name )
   {
      this.name = name;
   }
}
