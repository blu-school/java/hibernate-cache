package com.example.distributedcache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;

@SpringBootApplication( exclude = HazelcastAutoConfiguration.class )
public class DistributedCacheApplication
{

   public static void main( String[] args )
   {
      SpringApplication.run( DistributedCacheApplication.class, args );
   }

}
