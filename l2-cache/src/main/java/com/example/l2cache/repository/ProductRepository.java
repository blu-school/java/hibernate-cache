package com.example.l2cache.repository;

import com.example.l2cache.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository< Product, Long >
{
   @Modifying( flushAutomatically = true )
   @Query( value = "delete from Product p where p.id =:id" )
   void deleteProductWithDmlQuery( @Param( "id" ) Long id );

   @Modifying( flushAutomatically = true )
   @Query( value = "delete from Product p where p.id =:id", nativeQuery = true )
   void deleteProductWithNativeQuery( @Param( "id" ) Long id );

   @Modifying( flushAutomatically = true )
   @Query( value = "delete from Product_Product_Attributes ppa where ppa.product_id =:id", nativeQuery = true )
   void deleteProductAttributesWithNativeQuery( @Param( "id" ) Long id );
}
