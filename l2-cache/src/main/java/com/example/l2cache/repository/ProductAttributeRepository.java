package com.example.l2cache.repository;

import com.example.l2cache.model.ProductAttribute;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductAttributeRepository extends JpaRepository< ProductAttribute, Long >
{
   public static final String PRODUCT_ATTRIBUTES_BY_NAME_CACHE = "productAttributesByName";

   @Cacheable(cacheNames = PRODUCT_ATTRIBUTES_BY_NAME_CACHE)
   ProductAttribute findOneByName( String name);
}
