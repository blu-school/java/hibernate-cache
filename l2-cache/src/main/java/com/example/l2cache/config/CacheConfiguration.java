package com.example.l2cache.config;

import com.example.l2cache.model.Product;
import com.example.l2cache.model.ProductAttribute;
import com.example.l2cache.repository.ProductAttributeRepository;
import java.time.Duration;
import javax.cache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;
import org.hibernate.cache.jcache.ConfigSettings;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CacheConfiguration
{
   private final javax.cache.configuration.Configuration< Object, Object > jcacheConfiguration;

   public CacheConfiguration()
   {
      jcacheConfiguration =
         Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder
               .newCacheConfigurationBuilder( Object.class, Object.class, ResourcePoolsBuilder.heap( 10 ) )
               .withExpiry( ExpiryPolicyBuilder.timeToLiveExpiration( Duration.ofSeconds( 3600 ) ) )
               .build()
         );
   }

   @Bean
   public HibernatePropertiesCustomizer hibernatePropertiesCustomizer( javax.cache.CacheManager cacheManager )
   {
      return hibernateProperties -> hibernateProperties.put( ConfigSettings.CACHE_MANAGER, cacheManager );
   }

   @Bean
   public JCacheManagerCustomizer cacheManagerCustomizer()
   {
      return cm -> {
         createCache( cm, Product.class.getName() );
         createCache( cm, ProductAttribute.class.getName() );
         createCache( cm, ProductAttributeRepository.PRODUCT_ATTRIBUTES_BY_NAME_CACHE );
      };
   }

   private void createCache( CacheManager cm, String cacheName )
   {
      javax.cache.Cache< Object, Object > cache = cm.getCache( cacheName );
      if( cache != null ) {
         cache.clear();
      }
      else {
         cm.createCache( cacheName, jcacheConfiguration );
      }
   }

}
