package com.example.l2cache.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache( usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE )
public class Product
{

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   Long id;

   String title;

   BigDecimal price;

   @ManyToMany
   @Cache( usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE )
   List< ProductAttribute > productAttributes = new ArrayList<>();

   public Product()
   {
   }

   public Product( final String title, final BigDecimal price, final ProductAttribute... productAttributes )
   {
      this.title = title;
      this.price = price;
      Arrays.stream( productAttributes ).forEach( this::addProductAttribute );
   }

   public Long getId()
   {
      return id;
   }

   public void setId( final Long id )
   {
      this.id = id;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle( final String title )
   {
      this.title = title;
   }

   public BigDecimal getPrice()
   {
      return price;
   }

   public void setPrice( final BigDecimal price )
   {
      this.price = price;
   }

   public List< ProductAttribute > getProductAttributes()
   {
      return productAttributes;
   }

   public void addProductAttribute( ProductAttribute productAttribute )
   {
      productAttributes.add( productAttribute );
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (!(o instanceof Product)) {
         return false;
      }
      return id != null && id.equals(((Product) o).id);
   }

   @Override
   public int hashCode() {
      // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
      return getClass().hashCode();
   }

}
