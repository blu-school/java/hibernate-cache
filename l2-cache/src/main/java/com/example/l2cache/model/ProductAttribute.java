package com.example.l2cache.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache( usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE )
public class ProductAttribute
{

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   Long id;

   String name;

   public ProductAttribute()
   {
   }

   public ProductAttribute( final String name )
   {
      this.name = name;
   }

   public Long getId()
   {
      return id;
   }

   public void setId( final Long id )
   {
      this.id = id;
   }

   public String getName()
   {
      return name;
   }

   public void setName( final String name )
   {
      this.name = name;
   }


   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (!(o instanceof ProductAttribute)) {
         return false;
      }
      return id != null && id.equals(((ProductAttribute) o).id);
   }

   @Override
   public int hashCode() {
      // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
      return getClass().hashCode();
   }
}
