package com.example.l2cache.controller;

import com.example.l2cache.model.Product;
import com.example.l2cache.model.ProductAttribute;
import com.example.l2cache.service.ProductService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( "/products" )
public class ProductController
{
   @Autowired
   private ProductService productService;

   @GetMapping( "/{id}" )
   public Product getProduct( @PathVariable Long id )
   {
      return productService.findById( id ).orElseThrow();
   }

   @GetMapping( "/attributes/{id}" )
   public ProductAttribute getProductAttribute( @PathVariable Long id )
   {
      return productService.findAttributeById( id ).orElseThrow();
   }

   @GetMapping( "/attributes/name/{name}" )
   public ProductAttribute getProductAttributeByName( @PathVariable String name )
   {
      return productService.findByName( name );
   }

   @GetMapping
   public List< Product > getProducts()
   {
      return productService.findAll();
   }

   @GetMapping( "/attributes" )
   public List< ProductAttribute > getProductAttributes()
   {
      return productService.findAttributes();
   }

   @DeleteMapping( "/dml/{id}" )
   public void deleteProductWithDmlQuery( @PathVariable Long id )
   {
      productService.deleteWithDmlQuery( id );
   }

   @DeleteMapping( "/native/{id}" )
   public void deleteProductsWithNativeQuery( @PathVariable Long id )
   {
      productService.deleteWithNativeQuery( id );
   }

   @DeleteMapping( "/{id}" )
   public void deleteProduct( @PathVariable Long id )
   {
      productService.delete( id );
   }

   @DeleteMapping( "/attributes/{name}" )
   public void clearProductAttributeCache( @PathVariable String name )
   {
      productService.clearAttributeCache( name );
   }
}
