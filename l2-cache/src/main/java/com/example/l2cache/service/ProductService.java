package com.example.l2cache.service;

import com.example.l2cache.model.Product;
import com.example.l2cache.model.ProductAttribute;
import com.example.l2cache.repository.ProductAttributeRepository;
import com.example.l2cache.repository.ProductRepository;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductService implements InitializingBean
{
   private final ProductRepository productRepository;

   private final ProductAttributeRepository productAttributeRepository;

   private final CacheManager cacheManager;

   public ProductService(
      final ProductRepository productRepository,
      final ProductAttributeRepository productAttributeRepository, final CacheManager cacheManager )
   {
      this.productRepository          = productRepository;
      this.productAttributeRepository = productAttributeRepository;
      this.cacheManager               = cacheManager;
   }

   public List< Product > findAll()
   {
      return productRepository.findAll();
   }

   public Optional< Product > findById( Long id )
   {
      return productRepository.findById( id );
   }

   public ProductAttribute findByName( String name )
   {
      return productAttributeRepository.findOneByName( name );
   }

   public void deleteWithDmlQuery( Long id )
   {
      productRepository.deleteProductWithDmlQuery( id );
   }

   public void deleteWithNativeQuery( Long id )
   {
      productRepository.deleteProductAttributesWithNativeQuery( id );
      productRepository.deleteProductWithNativeQuery( id );
   }

   public void delete( Long id )
   {
      productRepository.deleteById( id );
   }

   @Override
   public void afterPropertiesSet() throws Exception
   {
      var food  = new ProductAttribute( "food" );
      var drink = new ProductAttribute( "drink" );
      var vitB  = new ProductAttribute( "vitaminB" );
      var cow   = new ProductAttribute( "cow" );

      productAttributeRepository.saveAll( List.of( food, drink, vitB, cow ) );

      var products = List.of(
         new Product( "Cheese", BigDecimal.valueOf( 3.5 ), food, cow ),
         new Product( "Beer", BigDecimal.valueOf( 2 ), drink, vitB )
      );

      productRepository.saveAll( products );

   }

   public List< ProductAttribute > findAttributes()
   {
      return productAttributeRepository.findAll();
   }

   public Optional< ProductAttribute > findAttributeById( final Long id )
   {
      return productAttributeRepository.findById( id );
   }

   public void clearAttributeCache( String name )
   {
      Objects.requireNonNull( cacheManager.getCache( ProductAttributeRepository.PRODUCT_ATTRIBUTES_BY_NAME_CACHE ) ).evict( name );
   }
}
